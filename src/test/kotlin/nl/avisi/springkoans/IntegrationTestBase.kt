package nl.avisi.springkoans

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = [SpringKoansApplication::class])
class IntegrationTestBase {

    @LocalServerPort
    val port = 0

    val baseUrl: String
        get() = "http://localhost:$port/tasks-service"

    protected val restTemplate = RestTemplate()

    fun <T> get(uri: String, responseType: Class<T>): ResponseEntity<T> =
            try {
                restTemplate.getForEntity("$baseUrl$uri", responseType)
            } catch (e: HttpClientErrorException) {
                ResponseEntity(e.statusCode)
            }

    fun <REQUEST, RESPONSE> post(uri: String, request: REQUEST, responseType: Class<RESPONSE>): ResponseEntity<RESPONSE> =
            try {
                restTemplate.postForEntity("$baseUrl$uri", request, responseType)
            } catch (e: HttpClientErrorException) {
                ResponseEntity(e.statusCode)
            }

    fun <RESPONSE> delete(uri: String, responseType: Class<RESPONSE>): ResponseEntity<RESPONSE> =
            try {
                restTemplate.exchange("$baseUrl$uri", HttpMethod.DELETE, null, responseType)
            } catch (e: HttpClientErrorException) {
                ResponseEntity(e.statusCode)
            }

}
