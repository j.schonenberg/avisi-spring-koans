package nl.avisi.springkoans.tasks

import nl.avisi.springkoans.tasks.model.Task
import org.springframework.stereotype.Service

@Service
class TaskService {

    val tasks = listOf(
            Task("task-a", "Boodschappen doen"),
            Task("task-b", "Auto naar garage"),
            Task("task-c", "Zolder opruimen")
    )

    fun getAll(): List<Task> {
        return tasks
    }

}
