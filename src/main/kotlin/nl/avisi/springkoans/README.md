# Tasks Microservice

De Tasks-microservice is een service voor het beheren van een takenlijst. 
Gebruikers kunnen taken toevoegen, later ophalen, en uiteindelijk afronden (of verwijderen).

In onderstaande oefeninigen zal de functionaliteit van de microservice beetje bij beetje worden uitgebouwd.
Voor deze oefeningen is het niet nodig de taken te persisteren in een database. De taken kunnen als variabele worden bijgehouden in de TaskService.
Let bij elke oefening op eventuele randscenario's, bijbehorende foutafhandeling, en natuurlijk testcoverage.  

## Oefening 1
First things first: om een overzicht van de taken te kunnen tonen moet er een endpoint zijn waarmee alle taken kunnen worden opgevraagd: 

* Voeg een endpoint toe waarmee alle taken kunnen worden opgehaald


## Oefening 2 
Het ophalen van taken is pas echt handig als je zelf ook nieuwe taken toe kan voegen:  

* Maak een endpoint waarmee een taak toe kan worden gevoegd
* Maak een endpoint waarmee een taak kan worden verwijderd

Let hierbij op de volgende business rules:
* Een ID is een gegenereert UUID, de gebruiker mag deze niet zelf bepalen (gebruik voor het genereren van een UUID `UUID.randomUUID().toString()`).
* Om taken in de (fictieve) front-end fatsoenlijk te kunnen tonen, heeft `titel` een maximale lengte van 100 karakters. Het is daarnaast ook een verplicht veld, er moet dus wel _iets_ in staan.


## Oefening 3 
Naast het kunnen ophalen van alle taken, zou het ook fijn zijn om 1 specifieke taak op te kunnen halen. 
En wat als je de beschrijving van een eerder aangemaakte taak wil updaten?

* Breidt de API uit zodat je een specifieke taak op kan halen
* Breidt de API uit zodat je een bestaande taak kan updaten


## Oefening 4
Takenlijstjes zijn pas echt leuk wanneer je taken ook af kan vinken.

* Pas het `Task`-model aan zodat kan worden bijgehouden of een taak openstaand of afgerond is.
* Maak het mogelijk om via de API een `Task` als afgerond te markeren.


# Oefening 5
Om het beheren van taken wat makkelijker te maken is het handig om naar specifieke taken te kunnen zoeken. 

* Breidt de API uit zodat naar taken kan worden gezocht op basis van beschrijving. Als ik bijvoorbeeld zoek op "auto" wil ik taken als "Auto naar garage" en "Verzekering voor auto afsluiten" zien.


## Oefening 6
Soms moeten taken vóór een bepaald moment worden uitgevoerd. Dit moet men ook vast kunnen leggen bij een taak.  

* Voeg een due-date toe aan het Task-model.
* Bij het aanmaken van een taak mag de due-date nooit in het verleden liggen.
* Maak het mogelijk om *ook* op due-date te zoeken. Dit (evt) in combinatie met het zoeken op beschrijving.
* Voor het zoeken op due-date moet men kunnen zoeken naar taken met een due-date binnen een bepaald datumbereik (vanaf datun, tot datum).


## Oefening 7
Alle taken zijn belangrijk, maar sommige taken zijn net iets belangrijker dan anderen. 

* Breidt het Task-model uit met een `priority`-veld. In dit veld kan worden aangegeven of een taak lage, normale, of hoge prioriteit heeft.
* Taken krijgen standaard de normale prioriteit.
* Maak het ook mogelijk om te zoeken op `priority`. Deze filter moet gecombineerd kunnen worden met de andere zoekfilters (beschrijving, due date).




