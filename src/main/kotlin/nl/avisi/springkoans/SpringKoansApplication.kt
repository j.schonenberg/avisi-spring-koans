package nl.avisi.springkoans

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class SpringKoansApplication

fun main(args: Array<String>) {
    runApplication<SpringKoansApplication>(*args)
}

@Bean
fun objectMapper(): ObjectMapper {
    return ObjectMapper().registerModules()
}
